<%@ page import="java.time.LocalDateTime" %>
<%@ page import="java.util.Enumeration" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Map" %><%--
  Created by IntelliJ IDEA.
  User: mrozj
  Date: 3/30/2019
  Time: 9:37 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <style><%@include file="/css/exercises.css"%></style>
</head>
<body>

<div id="header-div" style="color: darkblue">
    <h1>
        Expression JSP
    </h1>

    <p>
        <%=LocalDateTime.now()%>
    </p>

    <p>
        <%=10 + 2%>
    </p>

</div>

<div>
    <h1>
        Skryptlet JSP
    </h1>
    <p>
            <% for (int i = 0; i < 10; i++) {%>

    <h1><% out.print(i); %></h1>

    <%}%>
    </p>

    <table style="border: 1px solid black">
        <thead>
        <tr>
            <td>Imię</td>
            <td>Nazwisko</td>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Maciek</td>
            <td>Nowak</td>
        </tr>
        <tr>
            <td>Katarzyna</td>
            <td>Kwiecińska</td>
        </tr>

        <%
            for (int i = 0; i < 3; i++) {
                out.print("<tr><td>" + i + "</td><td>" + i * i + "</td></tr>");
            }%>


        <%for (int i = 0; i < 3; i++) {%>
        <tr>
            <td><%= i%>
            </td>
            <td><% out.print(i);%></td>
        </tr>
        <%}%>


        </tbody>
    </table>


    <h1>Dyrektywy</h1>
    <%! private int visitCount = 0;%>

    Liczba wyświetleń: <%=visitCount%>
    <%
        visitCount++;
    %>

    <%
        String liczba1 = request.getParameter("Liczba1");
        String liczba2 = pageContext.getRequest().getParameter("Liczba2");

        if (liczba1 != null || liczba2 != null) {
            int first = Integer.parseInt(liczba1);
            int second = Integer.parseInt(liczba2);
            int result = first + second;
            out.print("Wynik to: " + result);
        } else out.println("Wpisz poprawne liczby");
    %>


    <%
        String productName = request.getParameter("productName");
        String productCount = pageContext.getRequest().getParameter("productCount");
        if (productName == null || productCount == null) {
            out.print("Podaj parametry");
        } else {
            int productCountInt = Integer.parseInt(productCount);

            Map<String, Integer> productsMap = (Map<String, Integer>) session.getAttribute("productsMap");

            if (productsMap == null) {
                HashMap<String, Integer> newProductsMap = new HashMap<>();
                newProductsMap.put(productName, productCountInt);
                session.setAttribute("productsMap", newProductsMap);

//                Integer max = productsMap.entrySet().stream()
//                        .mapToInt(stringIntegerEntry -> stringIntegerEntry.getValue())
//                        .max().orElse(0);
//
//                response.addCookie(new Cookie("maxPriceProduct", max.toString()));

            } else {
                productsMap.put(productName, productCountInt);
                session.setAttribute("productsMap", productsMap);
            }
        }
    %>

    <%--<%-testowanie-%>
    <%--Map<String, Integer> productsMapToDisplay = (Map<String, Integer>) session.getAttribute("productsMap");--%>
    <%--if (productsMapToDisplay == null){--%>
    <%--out.print("Nie ma żadnych punktów w liście.");--%>
    <%--} else {--%>
    <%--for (Map.Entry<String, Integer> mapEntity : productsMapToDisplay.entrySet()){--%>
    <%--out.print(mapEntity.getKey());--%>
    <%--}}--%>
    <%--%>--%>


    <table style="border: 1px black solid">
        <tbody style="border: 1px black solid">
        <%
            Map<String, Integer> productsMapToDisplay = (Map<String, Integer>) session.getAttribute("productsMap");
            if (productsMapToDisplay != null) {
                for (Map.Entry<String, Integer> mapEntity : productsMapToDisplay.entrySet()) {
                    out.print(mapEntity.getKey());
        %>
        <tr>
            <td>
                <% out.print(mapEntity.getKey());%>
            </td>
            <td>
                <% out.print(mapEntity.getValue());%>
            </td>
        </tr>

        <%}%>
        <%}%>
        </tbody>
    </table>

<%--<div>--%>
    <%--Musisz kupic najwięcej ${cookie.get("maxCountProduct")}--%>
<%--</div>--%>

<%--<jsp include page="embedded.jsp"/>--%>

<h1>Using beans:</h1>

<%--<jsp:forward page="register.jsp"/>--%>

<jsp:useBean    id="calculatorBean"
                class="demoJsp.MyCalculatorBean"
                scope="session"/>

<jsp:setProperty name="calculatorBean" property="multiplyNumber" value="10"/>
<jsp:setProperty name="calculatorBean" property="addingNumber" value="2"/>

<%
    int add = calculatorBean.add(100);
    int multi = calculatorBean.multiply(100);

    out.print("<br>");
    out.print(add);
    out.print("<br>");
    out.print(multi);
    out.print("<br>");

%>

</div>

<a href="http://google.pl">Google</a>

</body>
</html>
