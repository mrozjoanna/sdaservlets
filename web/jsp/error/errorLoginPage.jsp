<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
    <style>
        <%@include file="/css/login.css"%>
    </style>
</head>
<body>

<%@include file="/jsp2/pageHeader.jsp"%>
<%
    response.setStatus(401);
%>
<div style="font-size: 30px; color: red;">
    Niepoprawny login lub haslo
</div>
<%@include file="/jsp2/pageFooter.jsp"%>

</body>
</html>
